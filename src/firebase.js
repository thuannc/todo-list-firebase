import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCSkLF8FXXydONjSdQ3Ic38o_fU1LEY_I8",
    authDomain: "todo-list-8cc3d.firebaseapp.com",
    databaseURL: "https://todo-list-8cc3d.firebaseio.com",
    projectId: "todo-list-8cc3d",
    storageBucket: "todo-list-8cc3d.appspot.com",
    messagingSenderId: "694432560242",
    appId: "1:694432560242:web:cdfcaf6bf96e6564087eeb",
    measurementId: "G-82P384QD75"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
