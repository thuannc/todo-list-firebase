import axios from 'axios'
axios.defaults.baseURL = process.env.REACT_APP_BASE_URL_API

const request = ['get', 'post', 'put', 'delete'].reduce((result, method) => {
  result[method] = async (url, data = {}, configs = {}) => {
    const accessToken = window.localStorage.getItem('accessToken')
    if (accessToken) configs.headers.Authorization = `Bearer ${accessToken}`
    if (method === 'get') configs.params = data
    try {
      const response = await axios({
        method,
        url,
        data,
        ...configs
      })
      return response.data
    } catch (e) {
      if (e.response) throw e.response.data
      throw e
    }
  }
  return result
}, {})

export default request
