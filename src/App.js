import React from 'react';
import './App.scss';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Root from './pages/root';

function App() {
  return (
    <>
      <Root />
      <ToastContainer />
    </>
  );
}

export default App;
