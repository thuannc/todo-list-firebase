import React from 'react';
import './index.scss';

import Button from '../Button';

function Card({ data, onDelete }) {

    const handleDelete = () => { onDelete(data.id); }

    return (
        <div
            className='card-container'
            style={{ borderLeft: `5px solid ${data.color}` }}>
            <p>{data.description || '-'}</p>
            <Button
                className='--btn-delete'
                label='Delete'
                color='red'
                onClick={handleDelete}
            />
        </div>
    );
}

export default Card;
