import React from 'react';
import PropTypes from 'prop-types';
import './index.scss';


function Button({ id, label, color, onClick, className }) {

    const handleOnClick = () => { onClick(id) }

    const classNames = `button-container color-${color}`;
    return (
        <div className={`${classNames} ${className || ''}`} onClick={handleOnClick}>
            {label}
        </div>
    );
}

Button.propTypes = {
    id: PropTypes.any,
    label: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    onClick: () => { },
    label: 'unknown',
    color: 'primary',
};

export default Button;
