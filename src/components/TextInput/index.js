import React from 'react';
import './index.scss';


function TextInput(props) {
    const { id, placeholder, value } = props;

    const onChange = (event) => {
        props.onChange(id, event.target.value)
    }

    return (
        <input
            type='text'
            className='form-control'
            value={value || ''}
            onChange={onChange}
            {...{ id, placeholder }}
        />
    );
}

export default TextInput;
