import React from 'react';
import PropTypes from 'prop-types';
import HyperModal from 'react-hyper-modal';

import './index.scss';

function Modal(props) {
    const { isOpen, title, children, onClose } = props;

    const closeModal = () => {
        onClose();
    }

    return (
        <HyperModal
            isOpen={isOpen}
            requestClose={closeModal}
            classes={{
                // wrapperClassName: 'modal-custom',
                contentClassName: 'modal-custom'
            }}
        >
            <div className='modal-container'>
                <div className='modal-header'>
                    <h4>{title}</h4>
                </div>
                <div className='modal-content'>
                    {children}
                </div>
            </div>
        </HyperModal>

    );
}

Modal.propTypes = {
    isOpen: PropTypes.bool,
    title: PropTypes.string,
    onClose: PropTypes.func,
};

Modal.defaultProps = {
    isOpen: false,
    title: 'Confirm',
    onClose: () => { },
};

export default Modal;
