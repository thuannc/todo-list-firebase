import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import chroma from 'chroma-js';
import './index.scss';

const dot = (color = '#ccc') => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
        backgroundColor: color,
        borderRadius: 10,
        content: '" "',
        display: 'block',
        marginRight: 8,
        height: 10,
        width: 10,
    },
});

const colourStyles = {
    control: styles => ({
        ...styles,
        minWidth: 300,
        marginRight: 16,
        backgroundColor: 'white',
        border: 'none !important',
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected
                    ? data.color
                    : isFocused
                        ? color.alpha(0.1).css()
                        : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? chroma.contrast(color, 'white') > 2
                        ? 'white'
                        : 'black'
                    : data.color,
            cursor: isDisabled ? 'not-allowed' : 'default',

            ':active': {
                ...styles[':active'],
                backgroundColor: !isDisabled && (isSelected ? data.color : color.alpha(0.3).css()),
            },
        };
    },
    input: styles => ({ ...styles, ...dot() }),
    placeholder: styles => ({ ...styles, ...dot() }),
    singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
};

function CustomSelect(props) {
    const { id, options, value, onChange, styles } = props;

    const handleOnChange = (event) => {
        onChange(event)
    }

    return (
        <Select
            styles={{ ...styles, ...colourStyles, }}
            options={options}
            onChange={handleOnChange}
            {...{ id, value }}
        />
    );
}

CustomSelect.propTypes = {
    id: PropTypes.any,
    options: PropTypes.array,
    value: PropTypes.object,
    styles: PropTypes.object,
    onChange: PropTypes.func,
};

CustomSelect.defaultProps = {
    onChange: () => { },
    value: '',
    options: [],
    styles: {},
};

export default CustomSelect;
