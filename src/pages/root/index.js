import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import shortid from 'shortid';
import './index.scss';

import { todosActionTypes } from '../../redux/actions/todos';
import { TextInput, Card, Button, Modal, Select } from '../../components';
import { colourOptions } from '../../constant/constant';
import { isArray } from '../../utils/Utils';

const Root = (props) => {
    // store
    const dispatch = useDispatch()
    const { list, loading } = useSelector(state => state.todos) || {};

    // state
    const [modal, setModal] = useState(false);
    const [description, setDescription] = useState('');
    const [color, setColor] = useState(colourOptions[0]);
    const [actionId, setActionId] = useState(null);

    let listRef = null, wrapperRef = null;

    useEffect(() => {
        dispatch({ type: todosActionTypes.GET_LIST_TODO })
    }, [])

    // handler
    const onClick = (type, params) => {
        switch (type) {
            case 'ADD_TODO':
                if (description.trim()) {
                    let data = {
                        id: shortid.generate(),
                        description,
                        color: color.color,
                        createdDate: (new Date()).getTime(),
                    }
                    dispatch({ type: todosActionTypes.ADD_TODO, data });
                    setDescription('');
                }
                break;
            case 'DELETE_TODO':
                if (actionId) {
                    dispatch({ type: todosActionTypes.DELETE_TODO, id: actionId });
                }
                setModal(false);
                break;
            case 'OPEN_MODAL':
                setModal(true);
                setActionId(params);
                break;
            default:
                break;
        }
    }

    const handleScroll = () => {
        if (wrapperRef.clientHeight + wrapperRef.scrollTop >= listRef.clientHeight - 300) {
            //300px ~ 5 item
            console.log('loadmore');
            // if (!loading) dispatch({ type: todosActionTypes.GET_LIST_TODO });
        }
    }

    // render
    const _renderModal = () => {
        if (!modal) return;
        return (
            <Modal
                isOpen={modal}
                onClose={() => setModal(false)}
            >
                <div className='content-outer'>
                    <span>Delete item?</span>
                    <div className='btn-outer'>
                        <Button
                            label='Agree'
                            onClick={() => onClick('DELETE_TODO')}
                        />
                    </div>
                </div>
            </Modal>
        )
    }

    return (
        <div className='root-container'>
            <h2>Todo List</h2>
            <div className='action-outer'>
                <TextInput
                    id='add'
                    placeholder='Description...'
                    value={description}
                    onChange={(id, value) => setDescription(value)}
                />
                <Select
                    value={color}
                    options={colourOptions}
                    onChange={_color => setColor(_color)}
                />
                <Button
                    label='Add'
                    onClick={() => onClick('ADD_TODO')}
                />
            </div>
            <div
                className='list-wraper'
                ref={ref => { if (!wrapperRef) wrapperRef = ref }}
                onScroll={() => handleScroll()}
            >
                <div
                    className='list-container'
                    ref={ref => { if (!listRef) listRef = ref }}>
                    {isArray(list, true)
                        ? list.map((todo) => (
                            <Card
                                key={todo.id}
                                data={todo}
                                onDelete={(id) => onClick('OPEN_MODAL', id)}
                            />
                        ))
                        : <span style={{ marginTop: 32 }}>No data!</span>
                    }
                </div>
            </div>
            {_renderModal()}
        </div>
    );
}

export default Root;
