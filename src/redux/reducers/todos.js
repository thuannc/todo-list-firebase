import { todosActionTypes } from '../actions/todos'

const defaultState = {

  fetching: {},
  contents: {},
  error: null,
  loading: false,
  list: [],

}

const todosReducer = (state = defaultState, { type, payload }) => {
  switch (type) {
    case todosActionTypes.COMMON_SUCCESS:
      return { ...state, loading: false }
    case todosActionTypes.COMMON_FAILED:
      return { ...state, loading: false, error: payload }

    case todosActionTypes.GET_LIST_TODO:
      const { page, size } = payload || {};
      return { ...state, loading: true, list: page !== 1 ? state.list : [] }
    case todosActionTypes.GET_LIST_TODO_SUCCESS:
      return {
        ...state,
        loading: false,
        ...getListSuccess(state, payload),
      }

    case todosActionTypes.ADD_TODO:
      return { ...state, loading: true, error: null }
    case todosActionTypes.ADD_TODO_SUCCESS:
      return { ...state, loading: false, list: [payload, ...state.list] }
    case todosActionTypes.DELETE_TODO:
      return { ...state, loading: true, error: null }
    case todosActionTypes.DELETE_TODO_SUCCESS:
      let _list = state.list || [];
      let index = _list.findIndex(t => t.id == payload);
      if (index !== -1) _list.splice(index, 1);
      return { ...state, loading: false, list: _list }
    default:
      return state
  }
}

const getListSuccess = (state, payload) => {
  let pagination = {}
  return {
    list: [...state.list, ...payload],
    pagination,
  }
}

export default todosReducer
