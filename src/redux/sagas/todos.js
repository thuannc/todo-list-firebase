import { all, put, takeLatest } from 'redux-saga/effects';
import { todosActionTypes } from '../actions/todos';
import { toast } from 'react-toastify';

import firebase from '../../firebase';

const dbCollection = firebase.firestore().collection('demo');

function* getListTodos(action) {
  // const { page, size } = action;
  try {
    // let snapshot = yield dbCollection.orderBy('createdDate', 'desc').limit(25).get();
    let snapshot = yield dbCollection.orderBy('createdDate', 'desc').get();
    if (snapshot && snapshot.docs) {
      let response = snapshot.docs.map(doc => doc.data());
      yield put({ type: todosActionTypes.GET_LIST_TODO_SUCCESS, payload: response })
    } else throw snapshot;
  } catch (error) {
    console.log("function*getListTodos -> error", error)
    yield put({ type: todosActionTypes.COMMON_FAILED, error });
  }
}

export function* addTodos(action) {
  const { data } = action;
  try {
    yield dbCollection.doc(data.id).set(data);
    yield put({ type: todosActionTypes.ADD_TODO_SUCCESS, payload: data });
    toast.success('Add success!', { position: toast.POSITION.BOTTOM_RIGHT });
  } catch (error) {
    toast.error('Opps!', { position: toast.POSITION.BOTTOM_RIGHT });
    yield put({ type: todosActionTypes.COMMON_FAILED, error });
  }
}

export function* deleteTodos(action) {
  const { id } = action;
  try {
    yield dbCollection.doc(id).delete();
    yield put({ type: todosActionTypes.DELETE_TODO_SUCCESS, payload: id });
    toast.success('Delete success!', { position: toast.POSITION.BOTTOM_RIGHT, });
  } catch (error) {
    toast.error('Opps!', { position: toast.POSITION.BOTTOM_RIGHT, });
    console.log("function*deleteTodos -> error", error)
    yield put({ type: todosActionTypes.COMMON_FAILED, error });
  }
}

export default function* authSaga() {
  yield all([
    takeLatest(todosActionTypes.GET_LIST_TODO, getListTodos),
    takeLatest(todosActionTypes.ADD_TODO, addTodos),
    takeLatest(todosActionTypes.DELETE_TODO, deleteTodos)
  ])
}
