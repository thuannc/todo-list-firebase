export const todosActionTypes = {
  GET_LIST_TODO: 'GET_LIST_TODO',
  GET_LIST_TODO_SUCCESS: 'GET_LIST_TODO_SUCCESS',
  DELETE_TODO: 'DELETE_TODO',
  DELETE_TODO_SUCCESS: 'DELETE_TODO_SUCCESS',
  ADD_TODO: 'ADD_TODO',
  ADD_TODO_SUCCESS: 'ADD_TODO_SUCCESS',

  COMMON_FAILED: 'COMMON_FAILED',
  COMMON_SUCCESS: 'COMMON_SUCCESS',
}

export const getlistTodoAction = (payload) => ({
  type: todosActionTypes.GET_LIST_TODO,
  payload
})

export const addTodosAction = (error) => ({
  type: todosActionTypes.ADD_TODO,
  error
})

export const deleteTodosAction = (error) => ({
  type: todosActionTypes.DELETE_TODO,
  error
})
// export const fetchInfoFailed = (error) => ({
//   type: todosActionTypes.fetchInfoFailed,
//   error
// })