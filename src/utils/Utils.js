export function isUndefined(e) {
    switch (e) {
        case 'undefined': case 'NaN': case NaN: case undefined: case '': case null: case 'null': case false: case 'false': case 'Invalid date': return true;
        default: return false;
    }
}

export function isArray(value, isNotEmpty) {
    if (Array.isArray(value)) {
        if (isNotEmpty) {
            let min_length = isNumber(isNotEmpty) ? parseInt(isNotEmpty) : 0;
            return value.length > min_length ? true : false;
        } else {
            return true;
        }
    }
    return false;
}

export function isNumber(value) {
    if (typeof value === 'number') { return true; }
    if (value && value.toString().split('.') === 2 && typeof parseFloat(value) === 'number') { return true; }
    if (isNumeric(value)) { return true; }
    return false;
}

export function isNumeric(str) {
    if (isUndefined(str)) { return false; }
    if (str.toString().match(/^[0-9]+$/) === null) { return false; }
    return true;
}

